import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import "rxjs/add/operator/map";
import "rxjs/add/operator/timeout";
import { LoadingController, Events } from 'ionic-angular';
import { startTimeRange } from '@angular/core/src/profile/wtf_impl';

@Injectable()
export class ApiCallerProvider {
  token: any;

  private headers = new Headers();
  link: string = "http://www.thingslab.in/";
  loading: any;
  headnew: Headers;
  constructor(public http: Http, public loadingCtrl: LoadingController, public events: Events) {
 
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');

    if (this.token == undefined) {
      if (localStorage.getItem("AuthToken") != null) {
        this.token = localStorage.getItem("AuthToken");
        console.log("Headers token => ", this.token)
        this.headers.append('Authorization', this.token);
      }
    }
  }

  startLoading() {
    return this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      spinner: "bubbles"
    });
  }

  stopLoading() {
    return this.loading.dismiss();
  }

  _urlPassed(baseUrl) {
    return this.http.get(baseUrl, { headers: this.headers })
      .map(res => res.json());
  }

  updateShiftApi(payload) {
    return this.http.post(this.link + "rosterPlan/updateRosterPlan", payload, { headers: this.headers })
      .map(res => res.json());
  }

  getShiftsList(_supAdId) {
    return this.http.get(this.link + "employeeshift/getShiftListByUser?user=" + _supAdId, { headers: this.headers })
      .map(res => res.json());
  }

  getplansApi(_empID) {
    return this.http.get(this.link + "rosterPlan/getRoasterPlanbyEmployee?employee_id=" + _empID, { headers: this.headers })
      .map(res => res.json());
  }

  getdevicesApi(link) {
    return this.http.get(link, { headers: this.headers })
      .map(res => res.json());
  }

  getPoiCall(_id) {
    return this.http.get("https://www.oneqlik.in/trackroute/sbUsers/routePoi?user=" + _id, { headers: this.headers })
      .map(res => res.json());
  }

  loginCall(data) {
    return this.http.post(this.link + "users/LoginWithOtp", data, { headers: this.headers })
      .map(res => res.json());
  }

  getDeviceByUserCall(userdetails) {
    return this.http.get("http://13.126.36.205:3000/devices/getDeviceByUser?id=" + userdetails._id + "&email=" + userdetails.email, { headers: this.headers })
      .map(res => res.json());
  }

  getRecentNotifCall(_id) {
    return this.http.get(this.link + 'notifs/getRecent?user=' + _id, { headers: this.headers })
      .map(res => res.json());
  }

  getList(starttime, endtime, _id) {
    return this.http.get(this.link + 'schoolTrip/list_trips_shuttle?from_date=' + starttime + '&to_date=' + endtime +  '&school=' +_id, {headers: this.headers})
    .map(res => res.json());
  }

    bookSeat(_id) {
   return this.http.get(this.link + 'schoolTrip/bookSeat?trip=' +_id, {headers: this.headers})
   .map(res => res.json())
  }

  pushnotifyCall(pushdata) {
    return this.http.post(this.link + "users/PushNotification", pushdata, { headers: this.headers })
      .map(res => res.json());
  }

  getSchedules(_id) {
    if (this.token == undefined) {
      if (localStorage.getItem("AuthToken") != null) {
        this.token = localStorage.getItem("AuthToken");
        console.log("Headers token => ", this.token)
        this.headers.append('Authorization', this.token);
      }
    }
    console.log("headers=> " + JSON.stringify(this.headers))
    return this.http.get(this.link + "student/parentScheduled?id=" + _id, { headers: this.headers })
      .timeout(15000)
      .map(res => res.json());
  }

  tripStatusCall(data) {
    // this.headnew = new Headers({ 'Content-Type': 'application/json; charset=utf-8', 'Authorization': this.token });
    return this.http.post(this.link + 'schoolTrip/find', data, { headers: this.headers })
      .timeout(15000)
      .map(res => res.json());
  }

  stoppage_detail(_id, from, to, device_id) {
    return this.http.get(this.link + 'stoppage/stoppage_detail?uId=' + _id + '&from_date=' + from + '&to_date=' + to + '&device=' + device_id, { headers: this.headers })
      .timeout(15000)
      .map(res => res.json());
  }

  gpsCall(device_id, from, to) {
    return this.http.get(this.link + 'gps/v2?id=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
      .timeout(15000)
      .map(res => res.json());
  }

  getDistanceSpeedCall(device_id, from, to) {
    return this.http.get(this.link + 'gps/getDistanceSpeed?imei=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
      .timeout(15000)
      .map(res => res.json());
  }

  // Rahul update service/////
  getStudent(data) {

    return this.http.post("http://www.thingslab.in/student/find", data, { headers: this.headers })
      .map(res => res.json());

  }

  studentlistreportCall(data) {

    return this.http.post("http://www.thingslab.in/attendance/studentList", data, { headers: this.headers })
      .map(res => res.json());

  }

  ReportCall(data) {

    // return this.http.post("http://www.thingslab.in/attendance/studentDayWiseReport", data, { headers: this.headers })
    return this.http.post("http://www.thingslab.in/attendance/studentDayWiseReport1", data, { headers: this.headers })
      .map(res => res.json());

  }


  studenEdit(data) {

    return this.http.post("http://www.thingslab.in/student/edit", data, { headers: this.headers })
      .map(res => res.json());

  }

  getprofileApi(_id) {
    return this.http.get("http://www.thingslab.in/users/parentData?id=" + _id, { headers: this.headers })
      .timeout(5000)
      .map(res => res.json());
  }
  getProfilestudentApi(_id) {
    return this.http.get("http://www.thingslab.in/users/parentData?id=" + _id, { headers: this.headers })
      .timeout(5000)
      .map(res => res.json());
  }

  shareLivetrackCall(data) {
    return this.http.post("https://www.oneqlik.in/share", data, { headers: this.headers })
      .map(res => res.json());
  }

  // getTrackrouteCall(link) {
  //   return this.http.get(link, { headers: this.headers })
  //     .map(res => res.json());
  // }

  getTrackrouteCall(id, user) {
    // 5b0bfedc43b09153a28c0b65
    return this.http.get("http://www.thingslab.in/trackRoute/routepath/getRoutePathWithPoi?id=" + id + '&user=' + user, { headers: this.headers })
      .map(res => res.json());
  }

  pullnotifyCall(pushdata) {
    return this.http.post("http://www.thingslab.in/users/PullNotification", pushdata, { headers: this.headers })
      .map(res => res.json());
  }
}

