// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Platform, AlertController } from 'ionic-angular';
import { Network } from '@ionic-native/network';


@Injectable()
export class ConnectivityServiceProvider {
  onDevice: boolean;
  constructor(
    public platform: Platform,
    public alertCtrl: AlertController,
    private network: Network) {
    console.log('Hello ConnectivityServiceProvider Provider');
    this.onDevice = this.platform.is('cordova');
  }

  isOnline(): boolean {
    if (this.onDevice && this.network.type) {
      return this.network.type !== 'none';
    } else {
      return navigator.onLine;
    }
  }

  isOffline(): boolean {
    if (this.onDevice && this.network.type) {
      return this.network.type === 'none';
    } else {
      return !navigator.onLine;
    }
  }

  // watch network for a connection
  // let connectSubscription = this.network.onConnect().subscribe(() => {
  //   console.log('network connected!');

  //   setTimeout(() => {
  //     if (this.network.type === 'wifi') {
  //       console.log('we got a wifi connection, woohoo!');
  //     }
  //   }, 3000);
  // });

}
