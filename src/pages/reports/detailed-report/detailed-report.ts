import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiCallerProvider } from '../../../providers/api-caller/api-caller';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-detailed-report',
  templateUrl: 'detailed-report.html',
})
export class DetailedReportPage implements OnInit {

  prePageData: any;
  isstudent: string;
  chidReport: any[]= [];
  datetimeStart: string;
  startDate: moment.Moment;
  endDate: moment.Moment;
  nameofmonth: any;
  childData: any = [];
  childName: any;
  someRep: any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public apiCall: ApiCallerProvider) {
    this.prePageData = this.navParams.get('data');
    this.childData = navParams.get("param");
    this.childName = navParams.get('childName').name;
    console.log("data of child", JSON.stringify(this.childName))
    this.nameofmonth = this.childData.monthString;
    this.startDate = moment([this.childData.year, this.childData.month - 1]);
    this.endDate = moment(this.startDate).endOf('month');


    //  for (var i = 0; i < this.prePageData.statusReport.length; i++) {
    //     // this.nameofmonth= this.prePageData.statusReport[i].monthString
    //     // console.log("student data name",this.prePageData.statusReport[i].year);

    //     this.startDate = moment([this.prePageData.statusReport[i].year, this.prePageData.statusReport[i].month - 1]);

    //     // Clone the value before .endOf()
    //     this.endDate = moment(this.startDate).endOf('month');
    //     console.log("start",this.startDate.toISOString());
    //     console.log('end',this.endDate.toISOString());
    //   }



    // just for demonstration:

    // console.log("start",this.startDate);
    // console.log('end',this.endDate);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailedReportPage');
  }

  ngOnInit() {
    this.ShowReportData();
  }

  ShowReportData() {
    // var data =
    // {
    //   "studentId": { 
    //     "_eval": "Id", 
    //     "value": this.prePageData.studentId 
    //   },
    //   "start": { 
    //     "_eval": "dayStart", 
    //     "value": this.startDate.toISOString() 
    //   },
    //   "end": { 
    //     "_eval": "dayEnd", 
    //     "value": this.endDate.toISOString()
    //   }
    // }
    var data = { "student": this.prePageData.studentId, "from": this.startDate.toISOString(), "to": this.endDate.toISOString() };
    // var data = { "student": "5cbac90372e9f217b8409fcd", "from": this.startDate.toISOString(), "to": this.endDate.toISOString() };
    this.apiCall.startLoading().present();
    this.apiCall.ReportCall(data)
      .subscribe(data => {
        console.log("student report data", data);

        for(var i = 0; i < data.length; i++) {
          this.someRep = {};
          if(data[i].event1Type == 'IN' && data[i].event2Type == 'IN') {
            this.someRep.pickupEv = 'IN';
            this.someRep.pickupTime = data[i].event1Time;
          } else if(data[i].event1Type == 'IN' && data[i].event2Type == 'OUT') {
            this.someRep.pickupEv = 'IN';
            this.someRep.pickupTime = data[i].event1Time;
            this.someRep.dropEv = 'OUT';
            this.someRep.dropTime = data[i].event2Time;
          } else if(data[i].event1Type == 'OUT' && data[i].event2Type == 'OUT') {
            this.someRep.dropEv = 'OUT';
            this.someRep.dropTime = data[i].event1Time;
          } else if(data[i].event1Type == 'OUT' && data[i].event2Type == 'IN') {
            this.someRep.pickupEv = 'IN';
            this.someRep.pickupTime = data[i].event2Time;
            this.someRep.dropEv = 'OUT';
            this.someRep.dropTime = data[i].event1Time;
          }
          this.chidReport.push(this.someRep);
        }
        console.log("student report", this.chidReport);

        this.apiCall.stopLoading();

        localStorage.setItem('student', data);
        this.isstudent = localStorage.getItem('student');
      },
        error => {
          this.apiCall.stopLoading();
          console.log(error);
        });
  }
}
