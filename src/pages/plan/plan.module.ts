import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlanPage } from './plan';
// import { IonCalendarModule } from '@ionic2-extra/calendar';

@NgModule({
  declarations: [
    PlanPage,
  ],
  imports: [
    IonicPageModule.forChild(PlanPage),
    // IonCalendarModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  // providers: [TranslatePipe, TranslateService]
})
export class PlanPageModule { }
