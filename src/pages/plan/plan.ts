import { Component, OnInit } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { NavController, AlertController } from 'ionic-angular';
import * as moment from 'moment';
import { ApiCallerProvider } from '../../providers/api-caller/api-caller';

@IonicPage()
@Component({
  selector: 'page-plan',
  templateUrl: 'plan.html',
})
export class PlanPage implements OnInit {

  _commonVar: any = {};

  constructor(
    private alertCtrl: AlertController,
    public navCtrl: NavController,
    public apiCall: ApiCallerProvider
  ) {
    this._commonVar._plansD = [];
    this._commonVar._isLogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> " + JSON.stringify(this._commonVar._isLogin));

    var curr = new Date();
    this._commonVar.firstday = new Date(curr.setDate(curr.getDate() - curr.getDay() + 1));
    this._commonVar.lastday = new Date(curr.setDate(curr.getDate() - curr.getDay() + 7));

    this._commonVar._today = new Date();
    this._commonVar._yesterday = new Date(new Date().setDate(new Date().getDate() - 1));
  
    this._commonVar.comDate1 = moment(this._commonVar._today, 'DD/MM/YYYY').format('MM/DD/YYYY');
    this._commonVar.comDate2 = moment(this._commonVar._yesterday, 'DD/MM/YYYY').format('MM/DD/YYYY');

    if (this._commonVar.comDate1 > this._commonVar.comDate2) {
      this._commonVar._showBTN = false;
    } else {
      this._commonVar._showBTN = true;
    }
    // console.log({
    //   firstday: firstday,
    //   lastday: lastday,
    //   today: new Date(),
    // });
  }

  ngOnInit() {
    this._getShifts();
    var refresher;
    this.getPlans(refresher);
  }

  getPlans(refresher) {
    this._commonVar.locationEndAddress = undefined;
    this.apiCall.startLoading();
    this.apiCall.getplansApi(this._commonVar._isLogin._id)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this._commonVar._plansD = [];
        var i = 0, howManyTimes = data.length;
        let that = this;
        function f() {
          var d1 = data[i].shift.name;
          var d2 = d1.split('-');
          var date1 = moment(new Date(data[i].schedule_Date), 'DD/MM/YYYY').format('DD MMM YY');
          var date2 = date1.split(' ');
          var dt1 = moment(that._commonVar.firstday, 'DD/MM/YYYY').format('MM/DD/YYYY');
          var dt2 = moment(that._commonVar.lastday, 'DD/MM/YYYY').format('MM/DD/YYYY');
          var dt3 = moment(new Date(data[i].schedule_Date), 'DD/MM/YYYY').format('MM/DD/YYYY');
          
          if (dt3 >= dt1 && dt3 <= dt2) {
            console.log("found dates in this week: ", data[i])
            that._commonVar._plansD.push(
              {
                _shiftName1: d2[0],
                _shiftName2: d2[1],
                _startT: data[i].shift.start_time,
                _endT: data[i].shift.end_time,
                _id: data[i]._id,
                _shiftID: data[i].shift._id,
                _scheduleDate: data[i].schedule_Date,
                _date1: date2[0],
                _date2: date2[1] + ' ' + date2[2],
                _status: data[i].status,
                _changeShift: false
              });
          }

          i++;
          if (i < howManyTimes) {
            setTimeout(f, 100);
          }
        }
        f();
        // if(this._commonVar._plansD.length )
        if (refresher) {
          setTimeout(() => {
            console.log('Async operation has ended');
            refresher.complete();
          }, 200);
        }

        console.log("overall plans: " + JSON.stringify(this._commonVar._plansD));
      },
        err => {
          this.apiCall.stopLoading();
          console.log("getting errors: ", err)
        });
  }

  _editPlan(plan, index) {
    console.log("selected plan to edit: ", index)
    this._commonVar._plansD[index]._changeShift = true;
  }

  _shiftChanged(shift) {
    console.log("selected shift: ", shift)
    this._commonVar._selectedShiftId = shift._id;
  }

  _getShifts() {
    this._commonVar._shifts = [];
    console.log("sup admin: " + this._commonVar._isLogin.supAdmin)
    this.apiCall.getShiftsList(this._commonVar._isLogin.supAdmin)
      .subscribe((data) => {
        this._commonVar._shifts = data;
        console.log("shifts list: ", data);
      },
        err => {
          console.log("got err: ", err)
        })
  }

  _savePlan(plan) {
    console.log("update clicked: ", plan)
    var payload = {
      user: this._commonVar._isLogin._id,
      shift: this._commonVar._selectedShiftId,
      plan_id: plan._id
    }
    this.apiCall.startLoading();
    this.apiCall.updateShiftApi(payload)
      .subscribe(data => {
        this.apiCall.stopLoading();
        console.log("resp update: ", data)
        var refresher;
        this.getPlans(refresher);
        this._getShifts();
      },
        err => {
          this.apiCall.stopLoading();
          console.log("some error: ", err)
        })
  }

  doRefresh(refresher) {
    this.getPlans(refresher);

  }

  _nextWeek(key) {
    let that = this;
    var refresher;
    
    if (key == 'priviousweek') {
      
      that._commonVar._yesterday = new Date(that._commonVar._yesterday.setDate(that._commonVar._yesterday.getDate() - 1));
      that._commonVar.comDate2 = moment(that._commonVar._yesterday, 'DD/MM/YYYY').format('MM/DD/YYYY');

      if (that._commonVar.comDate1 > that._commonVar.comDate2) {
        that._commonVar._showBTN = false;
      } else {
        that._commonVar._showBTN = true;
      }
      that._commonVar.firstday = new Date(that._commonVar.firstday.setDate(that._commonVar.firstday.getDate() - that._commonVar.firstday.getDay() - 6));
      that._commonVar.lastday = new Date(that._commonVar.lastday.setDate(that._commonVar.lastday.getDate() - that._commonVar.lastday.getDay() - 7));
      that.getPlans(refresher);
    } else if (key == 'nextweek') {
      
      that._commonVar._yesterday = new Date(that._commonVar._yesterday.setDate(that._commonVar._yesterday.getDate() + 1));
      that._commonVar.comDate2 = moment(that._commonVar._yesterday, 'DD/MM/YYYY').format('MM/DD/YYYY');

      if (that._commonVar.comDate1 > that._commonVar.comDate2) {
        that._commonVar._showBTN = false;
      } else {
        that._commonVar._showBTN = true;
      }

      that._commonVar.firstday = new Date(that._commonVar.firstday.setDate(that._commonVar.firstday.getDate() - that._commonVar.firstday.getDay() + 8));
      that._commonVar.lastday = new Date(that._commonVar.lastday.setDate(that._commonVar.lastday.getDate() - that._commonVar.lastday.getDay() + 7));
      that.getPlans(refresher);
    }
  }

  _cancelPlan(plan, index) {
    let alert = this.alertCtrl.create({
      title: "Confirm",
      message: "Do you want to cancel this plan?",
      buttons: [{
        text: 'YES PROCEED',
        handler: () => {
          var payload = {
            user: this._commonVar._isLogin._id,
            plan_id: plan._id,
            status: 'CANCELLED'
          }
          this.apiCall.startLoading();
          this.apiCall.updateShiftApi(payload)
            .subscribe(data => {
              this.apiCall.stopLoading();
              console.log("resp update: ", data)
              var refresher;
              this.getPlans(refresher);
              this._getShifts();
            },
              err => {
                this.apiCall.stopLoading();
                console.log("some error: ", err)
              })
        }
      },
      {
        text: "BACK"
      }]
    });
    alert.present();

  }

}
