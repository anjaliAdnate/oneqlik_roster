import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShuttlePage } from './shuttle';

@NgModule({
  declarations: [
    ShuttlePage,
  ],
  imports: [
    IonicPageModule.forChild(ShuttlePage),
  ],
})
export class ShuttlePageModule {}
