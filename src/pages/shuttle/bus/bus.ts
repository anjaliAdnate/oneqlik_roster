import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-shuttle',
  templateUrl: 'bus.html',
})
export class BusPage {

  constructor( public toastCtrl: ToastController,public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShuttlePage');
  }

  book() {
this.alertCtrl.create({
  message: 'Do you want to book the bus seat?',
  buttons: [{
    text: 'Yes',
    handler: () => {
      this.showToast();
    }
  },
{
  text: 'Cancle'
}]
}).present();
  }

  showToast() {
this.toastCtrl.create({
  message: 'Your request has successfully processed..!',
  duration: 1500,
  position: 'middle'
}).present();
  }

}