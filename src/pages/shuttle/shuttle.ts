import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, Events } from 'ionic-angular';
import { ApiCallerProvider } from '../../providers/api-caller/api-caller';
import * as moment from 'moment'

@IonicPage()
@Component({
  selector: 'page-shuttle',
  templateUrl: 'shuttle.html',
})
export class ShuttlePage implements OnInit {
  loginDetails: any = {}
  deviceUUID: any;
  datetimeStart: string;
  datetimeEnd: string;
  listdata: any;
  msg:  any;

  constructor( public toastCtrl: ToastController,public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams,
    public events: Events,
    public httpUrl: ApiCallerProvider) {
    console.log("login details=> " + localStorage.getItem('details'));
    this.loginDetails = JSON.parse(localStorage.getItem('details'));
    this.datetimeStart = moment({ hours: 0 }).format();
    console.log("today time: ", this.datetimeStart)
    this.datetimeEnd = moment({ hours: 0 }).add(15, 'days').format();
    console.log("after 15 day", this.datetimeEnd);
    // this.token = localStorage.getItem("DEVICE_TOKEN");
    this.deviceUUID = localStorage.getItem("UUID");
    console.log("UUID=> ", this.deviceUUID)

    // this.events.subscribe('cart:updated', (count) => {
    //   this.cartCount = count;
    // });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShuttlePage');
  }

  ngOnInit() {
    this.getList()
  }

 getList () {
  console.log("enter it")
  let that = this;
  this.httpUrl.getList(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), this.loginDetails.school)
  .subscribe(data => {
    //console.log("cheack data", data);
    this.listdata = data;
    console.log("final data", this.listdata);
    //console.log("absent", this.listdata.capacity);
    //console.log("absent one", this.listdata.seatBooked);
    for(let i=0;i<this.listdata.length;i++){
      var t = new Date(this.listdata[i].endTime);
      this.listdata[i].endTimeHHMM =  t.toLocaleTimeString();
      var a = (this.listdata[i].Device.capacity);
      console.log("capacity", a);
      var b = this.listdata[i].seatBooked;
      console.log("seat booked", b);
      var c = a - b;
      console.log(c);
      this.listdata[i].Device['availableSeat'] = c;
      console.log("seat avail", this.listdata[i].Device['availableSeat']);
    }
  })
}


  book(item) {
this.alertCtrl.create({
  message: 'Do you want to book the bus seat?',
  buttons: [{
    text: 'Yes',
    handler: () => {
      debugger
      this.httpUrl.bookSeat(item._id)
      .subscribe(data => {
      console.log("confirmation data", data);
      this.msg = data.message;
      if (this.msg == "booked successfully") {
  //     this.httpUrl.getList(new Date().toISOString(), new Date(this.datetimeEnd).toISOString(), this.loginDetails.school)
  //     .subscribe(data => {
  //     console.log("changing in seat", data);
  // })  
        this.getList();
      }
      console.log(this.msg);
      this.showToast(this.msg)
  })
    }
  },
{
  text: 'Cancel'
}]
}).present();

  }

  showToast(msg) {
this.toastCtrl.create({
  message: msg,
  //message: "booked successfully",
  duration: 1500,
  position: 'top'
}).present();
  }

}
