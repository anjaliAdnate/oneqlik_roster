import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-shuttle',
  templateUrl: 'cab.html',
})
export class CabPage {

  constructor( public toastCtrl: ToastController,public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShuttlePage');
  }

  book() {
this.alertCtrl.create({
  message: 'Do you want to book cab seat?',
  buttons: [{
    text: 'Yes',
    handler: () => {
      this.showToast();
    }
  },
{
  text: 'Cancle'
}]
}).present();
  }

  showToast() {
this.toastCtrl.create({
  message: 'Your request has successfully processed..!',
  duration: 1500,
  position: 'middle'
}).present();
  }

}