import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { NavController, NavParams, ActionSheetController, ModalController, IonicPage, Platform, ToastController, PopoverController, ViewController } from 'ionic-angular';
import { ApiCallerProvider } from '../../providers/api-caller/api-caller';
import * as io from 'socket.io-client';
import * as _ from "lodash";
import { SocialSharing } from '@ionic-native/social-sharing';
import { GoogleMaps, Marker, LatLng, Spherical, Polyline } from '@ionic-native/google-maps';
declare var google: any;

@IonicPage()
@Component({
    selector: 'page-live',
    templateUrl: 'live-single-device.html',
})
export class LiveSingleDevice implements OnInit, OnDestroy {
    _commonVar: any = {};
    _io: any;
    ongoingGoToPoint: any = {};
    ongoingMoveMarker: any = {};
    allData: any = {};
    socketSwitch: any = {};

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public apiCall: ApiCallerProvider,
        public actionSheetCtrl: ActionSheetController,
        public elementRef: ElementRef,
        public modalCtrl: ModalController,
        public plt: Platform,
        private socialSharing: SocialSharing,
        public toastCtrl: ToastController,
        public popoverCtrl: PopoverController
    ) {
        this._commonVar.callFunc = true;
        this._commonVar._finalizedData = [];
        this._commonVar.mapHideTraffic = false;
        this._commonVar.mapTrail = false;
        this._commonVar.socketChnl = [];
        this._commonVar._routeIndexData = [];
        this._commonVar._bounds = [];
        this._commonVar.AIR_PORTS = [];
        this._commonVar._totEmp = [];
        this._commonVar.condition = 'col1';
        this._commonVar.condition1 = 'light';
        this._commonVar.condition2 = 'light';
        this._commonVar._paramData = [];
        this._commonVar._mark = [];
        this._commonVar._isLoogin = JSON.parse(localStorage.getItem('details')) || {};

    }

    car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';

    carIcon = {
        path: this.car,
        labelOrigin: [25, 50],
        strokeColor: 'black',
        strokeWeight: .10,
        fillOpacity: 1,
        fillColor: 'blue',
        anchor: [10, 20], // orig 10,50 back of car, 10,0 front of car, 10,25 center of car,
    };

    icons = {
        "car": this.carIcon,
        "bike": this.carIcon,
        "truck": this.carIcon,
        "bus": this.carIcon,
        "user": this.carIcon,
        "jcb": this.carIcon,
        "tractor": this.carIcon
    }

    _loadMap(options) {
        let map = GoogleMaps.create('map_canvas_single_device', options);
        return map;
    }

    _getCompanyLocation() {
        this.apiCall.getprofileApi(this._commonVar._isLoogin._id)
            .subscribe(data => {
                console.log("comapny location: ", data);
                let that = this;
                that._commonVar._otp = data[0]._id.otp;
                var _payLoad = {
                    parent_id: data[0]._id._id,
                    routeIndex: (that._commonVar._routeIndexData[that._commonVar._routeIndexData.length - 1].routeIndex + 1),
                    routeMarkerLatitude: data[0].data[0].students.school.latLng.lat,
                    routeMarkerLongitude: data[0].data[0].students.school.latLng.lng,
                    studentName: data[0].data[0].students.school.schoolName,
                    student_id: data[0].data[0].students.school._id,
                }
                that._commonVar._routeIndexData.push(_payLoad);
                that._commonVar._finalizedData = that._commonVar._routeIndexData;
                this._commonVar._companyLocation = data[0];

                if (that._commonVar._data.scheduleschools.schoolVehicles.device.last_location != undefined) {
                    var _payLoad123 = {
                        parent_id: '',
                        routeIndex: (that._commonVar._routeIndexData[0].routeIndex - 1),
                        routeMarkerLatitude: that._commonVar._data.scheduleschools.schoolVehicles.device.last_location.lat,
                        routeMarkerLongitude: that._commonVar._data.scheduleschools.schoolVehicles.device.last_location.long,
                        studentName: 'Vehicle Location',
                        student_id: '',
                    }

                    that._commonVar._routeIndexData.splice(0, 0, _payLoad123);
                }

                console.log("finalized route index data: ", that._commonVar._routeIndexData)

                if (this._commonVar.callFunc)
                    this._firstFunc();

            })
    }

    ngOnInit() {
        var y = [];
        if (localStorage.getItem("paramData123") != null) {
            console.log("device details=> ", JSON.parse(localStorage.getItem("paramData123")));
            this._commonVar._paramData = JSON.parse(localStorage.getItem("paramData123"));
            this._commonVar.imei_id = localStorage.getItem("imei_ID");
            for (var r = 0; r < this._commonVar._paramData.routeIndexEmployee.length; r++) {
                if (this._commonVar._isLoogin._id == this._commonVar._paramData.routeIndexEmployee[r].parent_id) {
                    y = this._commonVar._paramData.routeIndexEmployee.splice(r);
                    this._commonVar._routeIndexData = y;
                }
            }
            this._commonVar._totEmpCount = y.length;
            console.log("y length: ", y.length)
            console.log("route index data: ", this._commonVar._routeIndexData)
        }
        this._getCompanyLocation();

        this._io = io.connect('http://www.thingslab.in/gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
        this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });

        // var paramData = this.navParams.get("data");
        var paramData = JSON.parse(localStorage.getItem("empData"));
        console.log('device data', paramData);

        this._commonVar._data = paramData;
        this._commonVar.routeId = paramData.scheduleschools.trackRoute._id;
    }

    ngOnDestroy() {
        let that = this;
        for (var i = 0; i < that._commonVar.socketChnl.length; i++)
            that._io.removeAllListeners(that._commonVar.socketChnl[i]);
        that._io.on('disconnect', () => {
            that._io.open();
        })
    }

    _firstFunc() {
        var mapOptions = {
            camera: {
                target: new LatLng(this._commonVar._routeIndexData[0].routeMarkerLatitude, this._commonVar._routeIndexData[0].routeMarkerLongitude),
            },
            gestures: {
                rotate: false,
                tilt: false,
                // scroll: false
            }
        };
        this._commonVar._map = this._loadMap(mapOptions);

        var lat_lng = new Array();
        // var labelOptions = {
        //     bold: true,
        //     fontSize: 15,
        //     color: "white"
        // };
        this.apiCall.startLoading().present();
        for (i = 0; i < this._commonVar._routeIndexData.length; i++) {
            var data123 = this._commonVar._routeIndexData[i];
            var myLatlng123 = new LatLng(data123.routeMarkerLatitude, data123.routeMarkerLongitude);
            this._commonVar._bounds.push(myLatlng123);
            this._commonVar._map.moveCamera({
                target: this._commonVar._bounds
            });
        }
        for (i = 0; i < this._commonVar._routeIndexData.length; i++) {
            var data = this._commonVar._routeIndexData[i];
            var myLatlng = new LatLng(data.routeMarkerLatitude, data.routeMarkerLongitude);
            // console.log("myLatLng: ", myLatlng)
            lat_lng.push(myLatlng);
            var schIconLoc, mapIcon;
            if (this.plt.is('android')) {
                schIconLoc = "./assets/imgs/pin_school_alt.png";
                mapIcon = "./assets/imgs/map-marker.png";
            } else if (this.plt.is('ios')) {
                schIconLoc = "www/assets/imgs/pin_school_alt.png";
                mapIcon = "www/assets/imgs/map-marker.png";
            }
            var icIcon = [];
            if (i == (this._commonVar._routeIndexData.length - 1)) {

                icIcon[i] = schIconLoc
            } else {
                if (this._commonVar._data.scheduleschools.schoolVehicles.device.last_location != undefined) {
                    if (i > 0) {
                        icIcon[i] = mapIcon
                    }
                } else {
                    icIcon[i] = mapIcon
                }
            }
            let that = this;
            if (this._commonVar._data.scheduleschools.schoolVehicles.device.last_location != undefined) {

                if (i > 0) {
                    let mark: Marker = this._commonVar._map.addMarkerSync({
                        position: myLatlng,
                        title: data.studentName,
                        icon: icIcon[i]
                    })
                    that._commonVar._mark.push({
                        marker: mark,
                        title: data.studentName
                    })
                }
            } else {
                let mark: Marker = this._commonVar._map.addMarkerSync({
                    position: myLatlng,
                    title: data.studentName,
                    icon: icIcon[i]
                })
                that._commonVar._mark.push({
                    marker: mark,
                    title: data.studentName
                })
            }
        }
        var start = lat_lng[0];
        var end = lat_lng[lat_lng.length - 1];
        var waypts = [];

        for (var i = 1; i < lat_lng.length - 1; i++) {
            waypts.push({
                location: lat_lng[i],
                stopover: true
            });
        }
        console.log("ways array: ", waypts)

        this.calcRoute(start, end, waypts);
    }

    calcRoute(start, end, waypts) {
        var directionsService = new google.maps.DirectionsService();
        let that = this;
        var request = {
            origin: start,
            destination: end,
            waypoints: waypts,
            optimizeWaypoints: true,
            travelMode: google.maps.TravelMode.DRIVING
        };

        directionsService.route(request, function (response, status) {

            if (status == google.maps.DirectionsStatus.OK) {
                var path = new google.maps.MVCArray();
                for (var i = 0, len = response.routes[0].overview_path.length; i < len; i++) {
                    path.push(response.routes[0].overview_path[i]);

                    that._commonVar.AIR_PORTS.push({
                        lat: path.j[i].lat(), lng: path.j[i].lng()
                    });

                    if (that._commonVar.AIR_PORTS.length > 1) {
                        that._commonVar._map.addPolyline({
                            'points': that._commonVar.AIR_PORTS,
                            'color': '#4aa9d5',
                            'width': 4,
                            'geodesic': true,
                        })
                    }
                }
                that.apiCall.stopLoading();
                that.socketInit(that._commonVar._data);
            }
        });
    }

    socketInit(pdata, center = false) {

        console.log("imei resend => ", pdata.scheduleschools.schoolVehicles.device.Device_ID)
        this._io.emit('acc', pdata.scheduleschools.schoolVehicles.device.Device_ID);
        this._commonVar.socketChnl.push(pdata.scheduleschools.schoolVehicles.device.Device_ID + 'acc');
        this._io.on(pdata.scheduleschools.schoolVehicles.device.Device_ID + 'acc', (d1, d2, d3, d4, d5) => {

            let that = this;
            if (d4 != undefined)
                (function (data) {

                    if (data == undefined) {
                        return;
                    }
                    if (data._id != undefined && data.last_location != undefined) {

                        var key = data._id;
                        let ic = _.cloneDeep(that.icons[data.iconType]);
                        if (!ic) {
                            return;
                        }
                        ic.path = null;
                        if (that.plt.is('ios')) {
                            // ic.url = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                            ic.url = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + 'bus.png';
                        } else {
                            if (that.plt.is('android')) {
                                // ic.url = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                                ic.url = './assets/imgs/vehicles/' + data.status.toLowerCase() + 'bus.png';
                            }
                        }
                        console.log("icon status 2 => ", ic.url)

                        if (that._commonVar.markObj != undefined) {
                            that._commonVar.markObj.remove();
                            console.log("marker removed");
                        }

                        if (that.allData[key]) {
                            that.socketSwitch[key] = data;
                            that.allData[key].mark.setIcon(ic);
                            that.allData[key].mark.setPosition(that.allData[key].poly[1]);
                            console.log("marker added on if condition: ", JSON.stringify(that.allData[key].mark))
                            var temp = _.cloneDeep(that.allData[key].poly[1]);
                            that.allData[key].poly[0] = _.cloneDeep(temp);
                            that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };
                            // var speed = data.status == "RUNNING" ? data.last_speed : 0;
                            that.liveTrack(that._commonVar._map, that.allData[key].mark, that.allData[key].poly, 50, 10, center, data._id, that);
                        }
                        else {
                            that.allData[key] = {};
                            that.socketSwitch[key] = data;
                            that.allData[key].poly = [];
                            if (data.sec_last_location) {
                                that.allData[key].poly.push({ lat: data.sec_last_location.lat, lng: data.sec_last_location.long });
                            } else {
                                that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                            }

                            if (data.last_location != undefined) {
                                if (that._commonVar.rememberMark) {
                                    that._commonVar.rememberMark.remove();
                                }
                                that._commonVar._map.addMarker({
                                    title: data.Device_Name,
                                    position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                                    icon: ic,
                                }).then((marker: Marker) => {
                                    console.log("marker added on else condition")
                                    that.allData[key].mark = marker;
                                    that.liveTrack(that._commonVar._map, that.allData[key].mark, that.allData[key].poly, 50, 10, center, data._id, that);
                                })
                            }
                        }
                    }
                })(d4)
        })
    }

    liveTrack(map, mark, coords, speed, delay, center, id, that) {

        var target = 0;

        clearTimeout(that.ongoingGoToPoint[id]);
        clearTimeout(that.ongoingMoveMarker[id]);
        // if (center) {
        //     map.setCameraTarget(coords[0]);
        // }
        function _goToPoint() {
            debugger
            console.log('mark', mark);
            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;
            console.log("lat: ", lat, " lng: ", lng)
            var step = (speed * 1000 * delay) / 3600000; // in meters
            // console.log("coords dest: ", JSON.stringify(coords))
            // if (coords != undefined) 
            //     var dest = new LatLng(coords[target].lat, coords[target].lng);
            // else
            //     return;

            if (coords[target])
                var dest = new LatLng(coords[target].lat, coords[target].lng);
            else
                return;

            var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
            console.log("distance: ", distance)
            var numStep = distance / step;
            console.log("numStep: ", numStep)
            var i = 0;
            var deltaLat = (coords[target].lat - lat) / numStep;
            console.log("deltaLat: ", deltaLat)
            var deltaLng = (coords[target].lng - lng) / numStep;
            console.log("deltaLng: ", deltaLng)
            function changeMarker(mark, head) {
                mark.setRotation(head);
            }

            function _moveMarker() {
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                if (i < distance) {
                    var head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
                    head = head < 0 ? (360 + head) : head;
                    console.log("head if: ", head)
                    // if ((head != 0) || (head != NaN)) {
                    //     changeMarker(mark, head);
                    // }
                    if (that._commonVar.mapTrail) {
                        that.showTrail(lat, lng, map);
                    } else {
                        if (that._commonVar.polylineID) {
                            that._commonVar.tempArray = [];
                            that._commonVar.polylineID.remove();
                        }
                    }

                    map.setCameraTarget(new LatLng(lat, lng));
                    that.latlngCenter = new LatLng(lat, lng);
                    mark.setPosition(new LatLng(lat, lng));
                    that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
                }
                else {
                    var head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
                    head = head < 0 ? (360 + head) : head;
                    console.log("head else: ", head)

                    // if ((head != 0) || (head != NaN)) {
                    //     changeMarker(mark, head);
                    // }
                    if (that._commonVar.mapTrail) {
                        that.showTrail(dest.lat, dest.lng, map);
                    } else {
                        if (that._commonVar.polylineID) {
                            that._commonVar.tempArray = [];
                            that._commonVar.polylineID.remove();
                        }
                    }
                    map.setCameraTarget(dest);
                    that.latlngCenter = dest;
                    mark.setPosition(dest);
                    target++;
                    that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
                }
            }
            _moveMarker();
        }
        _goToPoint();
    }

    shareLive() {
        let that = this;
        that._commonVar._showShareWindow = true;
    }

    showTrail(lat, lng, map) {
        let that = this;
        that._commonVar.tempArray.push({ lat: lat, lng: lng })
        if (that._commonVar.tempArray.length > 1) {
            map.addPolyline({
                points: that._commonVar.tempArray,
                color: '#0260f7',
                width: 5,
                geodesic: true
            }).then((polyline: Polyline) => {
                that._commonVar.polylineID = polyline;
            });
        }
    }

    sharedevices(param) {
        let that = this;
        if (param == '15mins') {
            that._commonVar.condition = 'col1';
            that._commonVar.condition1 = 'light';
            that._commonVar.condition2 = 'light';
            that._commonVar.tttime = 15;
        } else {
            if (param == '1hour') {
                that._commonVar.condition1 = 'col1';
                that._commonVar.condition = 'light';
                that._commonVar.condition2 = 'light';
                that._commonVar.tttime = 60;
            } else {
                if (param == '8hours') {
                    that._commonVar.condition2 = 'col1';
                    that._commonVar.condition = 'light';
                    that._commonVar.condition1 = 'light';
                    that._commonVar.tttime = (8 * 60);
                }
            }
        }
    }

    shareLivetemp() {
        // debugger
        let that = this;
        if (that._commonVar.tttime == undefined) {
            that._commonVar.tttime = 15;
        }
        var data = {
            id: that._commonVar._data.scheduleschools.schoolVehicles.device._id,
            imei: that._commonVar._data.scheduleschools.schoolVehicles.device.Device_ID,
            sh: that._commonVar._isLoogin._id,
            ttl: that._commonVar.tttime   // set to 1 hour by default
        };
        this.apiCall.startLoading().present();
        this.apiCall.shareLivetrackCall(data)
            .subscribe(data => {
                this.apiCall.stopLoading();
                this._commonVar.resToken = data.t;
                this.liveShare();
            },
                err => {
                    this.apiCall.stopLoading();
                    console.log(err);
                });
    }

    liveShare() {
        let that = this;
        var link = "https://www.thingslab.in/share/liveShare?t=" + that._commonVar.resToken;
        that.socialSharing.share(that._commonVar._isLoogin.fn + " " + that._commonVar._isLoogin.ln + " has shared " + that._commonVar._data.scheduleschools.schoolVehicles.device.Device_Name + " live trip with you. Please follow below link to track", "OneQlik Roster Trip", "", link);
        that._commonVar._showShareWindow = false;
        that._commonVar.tttime = undefined;
        let toast = this.toastCtrl.create({
            message: ""
        })
        toast.present();
    }

    onSelectMapOption(type) {
        let that = this;

        if (type == 'mapHideTraffic') {
            this._commonVar.mapHideTraffic = !this._commonVar.mapHideTraffic;
            if (this._commonVar.mapHideTraffic) {
                that._commonVar._map.setTrafficEnabled(true);
            } else {
                that._commonVar._map.setTrafficEnabled(false);
            }
        } else {
            if (type == 'mapTrail') {
                this._commonVar.mapTrail = !this._commonVar.mapTrail;
            }
        }

    }

    dialNumber(number) {
        window.open('tel:' + number, '_system');
    }

    openPopover(ev) {
        let that = this;
        console.log("check changing length: ", that._commonVar._finalizedData)
        let popover = this.popoverCtrl.create(PopoverPage, {
            "paramssomething": that._commonVar._finalizedData
        });

        popover.onDidDismiss(() => {
            that._commonVar.callFunc = false;
            this.ngOnInit();
        })

        popover.present({
            ev: ev
        });
    }

    zoomin() {
        let that = this;
        that._commonVar._map.moveCameraZoomIn();
    }
    zoomout() {
        let that = this;
        that._commonVar._map.moveCameraZoomOut();
    }
}

@Component({
    template: `
      <ion-list>
        <ion-item *ngFor="let per of _commonVar123._Data;let i = index;">
            <ion-avatar item-start>
                <img src="assets/imgs/dummy_user.jpg">
            </ion-avatar>
            <h2>{{per.studentName}}</h2>
        </ion-item>
      </ion-list>
    `
})
//   <ion-icon *ngIf="per.key == 'UP'" color="secondary" name="radio-button-on"></ion-icon>
//   <ion-icon *ngIf="per.key != 'UP'" color="danger" name="radio-button-on"></ion-icon>&nbsp;
export class PopoverPage implements OnInit {

    _commonVar123: any = {};
    constructor(
        navParams: NavParams,
        public modalCtrl: ModalController,
        public viewCtrl: ViewController
    ) {
        this._commonVar123.vehData = [];
        this._commonVar123._Data = [];
        this._commonVar123.vehData = navParams.get("paramssomething");
        console.log("popover data123=> ", this._commonVar123.vehData);

        this._commonVar123.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + JSON.stringify(this._commonVar123.islogin));
    }
    ngOnInit() {
        var y = [];
        debugger;
        for (var r = 1; r < this._commonVar123.vehData.length; r++) {
            if (this._commonVar123.islogin._id == this._commonVar123.vehData[r].parent_id) {
                y = this._commonVar123.vehData.splice(r + 1);
                this._commonVar123._Data = y;
            }
        }
        this._commonVar123._Data.pop();
        console.log("popover data=> ", this._commonVar123._Data);
    }
}
