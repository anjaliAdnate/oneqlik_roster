import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform, ToastController } from 'ionic-angular';
import { ApiCallerProvider } from '../../providers/api-caller/api-caller';
import { GoogleMap, GoogleMaps, LatLng, GoogleMapsEvent, Geocoder, GeocoderResult } from '@ionic-native/google-maps';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage implements OnInit {
  map: GoogleMap;
  profilesStudent: any;
  profilesParent: any;
  loginDetails: any;
  token: string;
  deviceUUID: string;
  profilesParentname: any;
  profilesParentphone: any;
  profilesParentemail: any;
  // location: any;
  mapElement: any;
  profilesParenterelation: any;
  allData: any = {};
  mapid: any;
  markerlatlong: any;
  // data: { "_id": any; "latLng": { "lat": any; "lng": any; }; };
  lattitude: string;
  longtitude: string;
  studentid: any;
  // stdn: { "_id": any; "latLng": { "lat": string; "lng": string; }; };
  address_show: any;
  locationShow: any;
  addressofstudent: any;
  schoolmapid: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public apicall: ApiCallerProvider, public alertCtrl: AlertController, public toastCtrl: ToastController) {
    console.log("login details=> " + localStorage.getItem('details'));
    this.loginDetails = JSON.parse(localStorage.getItem('details'));
    console.log("login _id=> " + this.loginDetails._id);
    this.token = localStorage.getItem("DEVICE_TOKEN");
    this.deviceUUID = localStorage.getItem("UUID");
  }
  ionViewDidLoad() {
    this.getProfile();
  }

  ngOnInit() {}

  getProfile() {
    this.apicall.startLoading().present();
    this.apicall.getprofileApi(this.loginDetails._id)
      .subscribe(data => {
        this.apicall.stopLoading();
        this.profilesStudent = data[0].data;
        this.profilesParentname = data[0]._id.first_name;
        this.profilesParentphone = data[0]._id.phone;
        this.profilesParentemail = data[0]._id.email;
        this.profilesParenterelation = data[0]._id.relation
        this.profilesParent = data[0]._id;
        for (var i = 0; i < data[0].data.length; i++) {
          this.studentid = "b" + i;
          (function (that, studentid) {
            var _studId = data[0].data[i].students._id;
            that.allData.map = GoogleMaps.create(studentid, {
              camera: {
                target: { lat: data[0].data[i].students.latLng.lat, lng: data[0].data[i].students.latLng.lng },
                zoom: 13
              }
            });
            that.allData.map.addMarker({
              title: 'Employee position',
              icon: 'green',
              animation: 'DROP',
              draggable: true,
              position: {
                lat: data[0].data[i].students.latLng.lat,
                lng: data[0].data[i].students.latLng.lng
              }
            })
              .then(marker => {
                marker.on(GoogleMapsEvent.MARKER_DRAG_END, studentid)
                  .subscribe(() => {
                    that.markerlatlong = marker.getPosition();
                    Geocoder.geocode({
                      "position": {
                        lat: that.markerlatlong.lat,
                        lng: that.markerlatlong.lng
                      }
                    }).then((results: GeocoderResult[]) => {
                      if (results.length == 0) {
                        return null;
                      }
                      
                      let address: any = [
                        results[0].subThoroughfare || "",
                        results[0].thoroughfare || "",
                        results[0].locality || "",
                        results[0].adminArea || "",
                        results[0].postalCode || "",
                        results[0].country || ""
                      ].join(", ");
                      that.addressofstudent = results[0].extra.lines[0];
                      that.sendAddress(that.addressofstudent, _studId, that.markerlatlong.lat, that.markerlatlong.lng);
                    });
                  });
              });
          })(this, this.studentid)
        };

        for (var i = 0; i < data[0].data.length; i++) {
          let that = this;
          that.schoolmapid = "a" + i;
          that.allData.map = GoogleMaps.create(that.schoolmapid, {
            camera: {
              target: { lat: data[0].data[i].students.school.latLng.lat, lng: data[0].data[i].students.school.latLng.lng },
              zoom: 10
            }
          });

          that.allData.map.addMarker({
            title: 'Company Location',
            icon: 'red',
            animation: 'DROP',
            draggable: false,
            position: {
              lat: data[0].data[i].students.school.latLng.lat,
              lng: data[0].data[i].students.school.latLng.lng
            }
          });
        }

        if (this.profilesStudent.length == 0) {
          let alert = this.alertCtrl.create({
            message: "No Data Found",
            buttons: ['OK']
          });
          alert.present();
        }
      }, error => {
        this.apicall.stopLoading();
        console.log(error);
      })
  }

  sendAddress(address, ID, Lat, Long) {
    var stdn = {
      "_id": ID,
      "latLng": { "lat": Lat, "lng": Long },
      "address": address
    }
    this.apicall.startLoading().present();
    this.apicall.studenEdit(stdn)
      .subscribe(data => {
        this.apicall.stopLoading();
        const toast = this.toastCtrl.create({
          message: 'Employee pickup location updated successfully.',
          duration: 3000,
          position: 'bottom'
        })
        toast.present();
        // this.getProfile();
      },
        error => {
          this.apicall.stopLoading();
          console.log(error);
        });
  }
}
