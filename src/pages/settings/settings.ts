import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, App } from 'ionic-angular';
import { AppVersion } from '@ionic-native/app-version';
import { ApiCallerProvider } from '../../providers/api-caller/api-caller';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  userdetails: any;
  notif: boolean;
  version: any;
  aVer: any;
  token: string;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public _app: App,
    private appVersion: AppVersion,
    public apiCall: ApiCallerProvider,
    private storage: Storage
  ) {
    this.userdetails = JSON.parse(localStorage.getItem('details'));
    if (localStorage.getItem("NOTIF") != null) {
      this.notif = JSON.parse(localStorage.getItem("NOTIF"));
    }
    this.appVersion.getVersionNumber().then((version) => {
      this.aVer = version;
      console.log("app version=> " + this.aVer);
    });

    this.person = { name: undefined, company: undefined, birthdate: undefined };
    this.dob = undefined;
  }

  ionViewDidLoad() {
    let person = JSON.parse(localStorage.getItem('PERSON'));
    if (person) {
      this.person = person;
      this.age = this.getAge(this.person.birthdate);
      this.dob = new Date(this.person.birthdate).toISOString();
    }
  }

  logout() {
    this.token = localStorage.getItem("DEVICE_TOKEN");
    var pushdata = {
      "uid": this.userdetails._id,
      "token": this.token,
      "os": "android",
      "app": "saftey365"
    }
    let alert = this.alertCtrl.create({
      message: 'Do you want to logout from the app?',
      buttons: [{
        text: 'YES',
        handler: () => {
          this.apiCall.startLoading().present();
          this.apiCall.pullnotifyCall(pushdata)
            .subscribe(data => {
              this.apiCall.stopLoading();
              console.log("push notifications updated " + data.message)
              localStorage.clear();
              this.storage.remove("DEVICE_TOKEN");
              this._app.getRootNav().setRoot('LoginPage');
            },
              err => {
                this.apiCall.stopLoading();
                console.log(err)
              });

        }
      },
      {
        text: 'No'
      }]
    });
    alert.present();
  }

  notifChange(value) {
    if (value == false) {
      let alert = this.alertCtrl.create({
        message: 'Do you want to stop getting notifications?',
        buttons: [{
          text: 'Yes',
          handler: () => {
            this.notif = value;
            localStorage.setItem("NOTIF", JSON.stringify(this.notif));
          }
        },
        {
          text: 'Back'
        }]
      });
      alert.present();
    } else {
      localStorage.setItem("NOTIF", JSON.stringify(value));
    }
  }

  public person: { name: string, company: string, birthdate?: number };
  dob: any;
  age: any;
  showProfile: boolean;

  reset() {
    this.person = { name: null, company: null, birthdate: null };
    this.dob = null;
    this.showProfile = false;
  }

  save() {
    this.person.birthdate = new Date(this.dob).getTime();
    this.age = this.getAge(this.person.birthdate);
    this.showProfile = true;
    localStorage.setItem('PERSON', JSON.stringify(this.person));
  }

  getAge(birthdate) {
    let currentTime = new Date().getTime();
    return ((currentTime - birthdate) / 31556952000).toFixed(0);
  }
}
