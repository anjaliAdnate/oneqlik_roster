import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, Platform, Events } from 'ionic-angular';
import { ApiCallerProvider } from '../../providers/api-caller/api-caller';
import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';
import { Subscription } from 'rxjs/Subscription';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import * as io from 'socket.io-client';   // with ES6 import

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage implements OnInit {
  loginDetails: any = {};
  schedules: any;
  token: any;
  deviceUUID: any;
  pushnotify: any;
  connected: Subscription;
  disconnected: Subscription;
  public cartCount: number = 0;
  socket: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpUrl: ApiCallerProvider,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public storage: Storage,
    private network: Network,
    private push: Push,
    public plt: Platform,
    public events: Events
  ) {
    console.log("login details=> " + localStorage.getItem('details'));
    this.loginDetails = JSON.parse(localStorage.getItem('details'));

    // this.token = localStorage.getItem("DEVICE_TOKEN");
    this.deviceUUID = localStorage.getItem("UUID");
    console.log("UUID=> ", this.deviceUUID)

    this.events.subscribe('cart:updated', (count) => {
      this.cartCount = count;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
    this.socket = io('http://www.thingslab.in/sbNotifIO', {
      transports: ['websocket', 'polling']
    });

    this.socket.on('connect', () => {
      console.log('IO Connected tabs');
      console.log("socket connected tabs", this.socket.connected)
    });

    this.socket.on(this.loginDetails._id, (msg) => {
      // this.notData.push(msg);
      this.cartCount += 1;
      console.log("tab notice data=> ")
    })
  }

  ionViewDidEnter() {
    this.connected = this.network.onConnect().subscribe(data => {
      console.log(data)
      // this.displayNetworkUpdate(data.type);
      this.getSchedules();
    }, error => console.log(error));

    this.disconnected = this.network.onDisconnect().subscribe(data => {
      this.displayNetworkUpdate(data.type);
      console.log(data);
    }, error => console.log(error));
  }

  ionViewWillLeave() {
    this.connected.unsubscribe();
    this.disconnected.unsubscribe();
  }

  seeNotifications() {
    this.navCtrl.push('NotificationPage');
  }

  displayNetworkUpdate(connectionState: string) {

    // let networkType = this.network.type;

    this.toastCtrl.create({
      // message: `You are now ${connectionState} via ${networkType}`,
      message: `You are now ${connectionState}`,
      duration: 3000,
      position: 'bottom'
    }).present();
  }

  ngOnInit() {
    this.getSchedules();
    localStorage.removeItem("imei_ID");
    localStorage.removeItem("paramData123");
    localStorage.removeItem("empData");
  }

  getSchedules() {
    this.httpUrl.startLoading().present();
    this.httpUrl.getSchedules(this.loginDetails._id)
      .subscribe(data => {
        this.httpUrl.stopLoading();
        console.log("data=> ", data);
        this.schedules = data;
        console.log("schedules data=> ", this.schedules);
        this.addPushNotify();
      },
        err => {
          this.httpUrl.stopLoading();
          // if (err.message == "Timeout has occurred") {
          //   this.timeoutAlert();
          // }
          console.log("error got=> ", err)
        });
  }

  timeoutAlert() {
    let alerttemp = this.alertCtrl.create({
      message: "Server is taking much time to respond. Please try in some time.",
      buttons: [{
        text: 'Okay',
        handler: () => {
          this.navCtrl.setRoot("TabsPage");
        }
      }]
    });
    alerttemp.present();
  }

  doRefresh(refresher) {
    // console.log('Begin async operation', refresher);
    this.getSchedules();
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 200);
  }

  pushSetup() {
    // to initialize push notifications
    let that = this;
    const options: PushOptions = {
      android: {
        senderID: '644983599736',
        icon: 'safety365',
        iconColor: 'red'
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'true'
      },
      windows: {},
      browser: {
        pushServiceURL: 'http://push.api.phonegap.com/v1/push'
      }
    };

    const pushObject: PushObject = that.push.init(options);

    pushObject.on('registration')
      .subscribe((registration: any) => {
        console.log("device token => " + registration.registrationId);
        localStorage.setItem("DEVICE_TOKEN", registration.registrationId)
        this.storage.set("DEVICE_TOKEN", registration.registrationId);
        this.addPushNotify();
      });

    pushObject.on('error').subscribe(error => {
      console.error('Error with Push plugin', error)
    });
  }

  addPushNotify() {
    // console.log("push notify click")
    this.storage.get("DEVICE_TOKEN").then((res) => {
      if (res) {
        console.log("device token=> " + JSON.stringify(res));
        this.token = res;
        var pushdata, osvar;
        if (this.plt.is('android')) {
          osvar = 'android';
          pushdata = {
            "uid": this.loginDetails._id,
            "token": this.token,
            "os": osvar,
            "appName": "Safety365"
          }
        } else {
          if (this.plt.is('ios')) {
            osvar = 'ios';
            pushdata = {
              "uid": this.loginDetails._id,
              "token": this.token,
              "os": osvar,
              "appName": "Safety365"
            }
          }
        }

        // console.log("pushdata=> " + JSON.stringify(pushdata));
        this.httpUrl.pushnotifyCall(pushdata)
          .subscribe(data => {
            this.pushnotify = data.message;
            console.log("data of push=> " + JSON.stringify(data));
          },
            error => {
              console.log(error);
            });
      } else {
        this.pushSetup();
      }
    });

    // debugger;
  }

  showTrip(data) {
    console.log('show Trip', data)

    var postData = {
      "_find": {
        "createdAt": {
          "$gte": {
            "_eval": "dayStart",
            "value": new Date().toISOString()
          }
        },
        "ScheduleSchool": {
          "_eval": "Id",
          "value": data._id
        }
      }
    }
    this.httpUrl.startLoading().present();
    this.httpUrl.tripStatusCall(postData)
      .subscribe(respData => {

        console.log('response data: ', respData)
        this.httpUrl.stopLoading();

        if (respData.length == 0) {
          const toast = this.toastCtrl.create({
            message: 'This trip is yet to start.. Please check afetr some time. Thank you!',
            duration: 3000,
            position: 'bottom'
          })
          toast.present();
        } else {
          if (respData[0].pickup == "START" || respData[0].drop == "START") {
            this.navCtrl.push("LiveSingleDevice", {
              imei_ID: data.scheduleschools.schoolVehicles.device.Device_ID,
              param: respData[0],
              data: data
            });
            localStorage.setItem("imei_ID", JSON.stringify(data.scheduleschools.schoolVehicles.device.Device_ID));
            localStorage.setItem("paramData123", JSON.stringify(respData[0])),
              localStorage.setItem("empData", JSON.stringify(data));
          } else {
            if (respData[0].pickup == "END" || respData[0].drop == "END") {
              this.navCtrl.push("HistoryMapPage", {
                imei_ID: data.scheduleschools.schoolVehicles.device.Device_ID,
                param: respData[0]
              });
            }
          }
        }
      },
        err => {
          this.httpUrl.stopLoading();
          console.log("error occured=> ", err)
        });
  }

}
