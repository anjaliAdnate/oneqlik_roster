import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, AlertController, App, ToastController, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { TextToSpeech } from '@ionic-native/text-to-speech';
import { ApiCallerProvider } from '../providers/api-caller/api-caller';
import { Uid } from '@ionic-native/uid';
import { Diagnostic } from '../../node_modules/@ionic-native/diagnostic';
import { Storage } from '@ionic/storage';
import { ShuttlePage } from '../pages/shuttle/shuttle';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { SettingsPage } from '../pages/settings/settings';
import { PlanPage } from '../pages/plan/plan';
import { ProfilePage } from '../pages/profile/profile';
import { ReportsPage } from '../pages/reports/reports';
export interface PageInterface {
  title: string;
  name: string;
  component: any;
  icon: string;
  logsOut?: boolean;
  index?: number;
  tabName?: string;
  tabComponent?: any;
}

@Component({
  selector: 'app-main-page',
  templateUrl: 'app.html'
})


export class MyApp {
  @ViewChild(Nav) nav: Nav;
  appPages: PageInterface[] = [
    // { title: 'Shuttle', name: 'TabsPage', component: TabsPage, icon: 'custom-bus' },
    { title: 'Home', name: 'TabsPage', component: HomePage, icon: 'home' },
    { title: 'Reports', name: 'ReportsPage', component: ReportsPage, icon: 'list-box' },
    { title: 'Profile', name: 'ProfilePage', component: ProfilePage, icon: 'person' },    
    { title: 'Plan', name: 'PlanPage', component: PlanPage, icon: 'calendar' },
    { title: 'Settings', name: 'SettingsPage', component: SettingsPage, icon: 'cog' },
  ];
  rootPage: any;
  islogin: any = {};
  token: any;
  isDealer: any;
  notfiD: boolean;
  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public events: Events,
    private push: Push,
    public alertCtrl: AlertController,
    public app: App,
    public apiCall: ApiCallerProvider,
    public toastCtrl: ToastController,
    private tts: TextToSpeech,
    private uid: Uid,
    private diagnostic: Diagnostic,
    public storage: Storage,
    public menu: MenuController,
  ) {

    platform.ready().then(() => {
      this.getPermission();
      statusBar.styleDefault();
      statusBar.hide();
      this.splashScreen.hide();
    });

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};

    this.initializeApp();

    if (localStorage.getItem("LOGGED_IN")) {
      this.rootPage = 'TabsPage';
    } else {
      this.rootPage = 'LoginPage';
    }

  }

  openPage(page: PageInterface) {
    let params = {};

    // the nav component was found using @ViewChild(Nav)
    // setRoot on the nav to remove previous pages and only have this page
    // we wouldn't want the back button to show in this scenario
    if (page.index) {
      params = { tabIndex: page.index };
    }

    // If we are already on tabs just change the selected tab
    // don't setRoot again, this maintains the history stack of the
    // tabs even if changing them from the menu
    if (this.nav.getActiveChildNavs().length && page.index != undefined) {
      this.nav.getActiveChildNavs()[0].select(page.index);
    } else {
      // Set the root of the nav with params if it's a tab index
      this.nav.setRoot(page.name, params).catch((err: any) => {
        console.log(`Didn't set nav root: ${err}`);
      });
    }

    if (page.logsOut === true) {
      // Give the menu time to close before changing to logged out
      // this.userData.logout();
    }
  }

  getPermission() {

    this.diagnostic.getPermissionAuthorizationStatus(this.diagnostic.permission.READ_PHONE_STATE).then((status) => {

      if (status != this.diagnostic.permissionStatus.GRANTED) {

        this.diagnostic.requestRuntimePermission(this.diagnostic.permission.READ_PHONE_STATE).then((data) => {
          // alert("granted data => " + data)
          var imei = this.uid.IMEI;
          // alert("in promise imei " + imei);
          console.log("in promise imei " + imei)
        })
      }
      else {
        console.log("We have the permission");
        // alert("IMEI granted ----------" + this.uid.IMEI);
        // alert("UUID granted ----------" + this.uid.UUID);
        // console.log("component uuid=> "+ this.uid.UUID)
        localStorage.setItem("IMEI", this.uid.IMEI);
        localStorage.setItem("UUID", this.uid.UUID);
      }
    }, (statusError) => {
      console.log(`statusError=> ` + statusError);
      // alert(statusError);
    });
  }

  pushNotify() {
    let that = this;
    that.push.hasPermission()                       // to check if we have permission
      .then((res: any) => {

        if (res.isEnabled) {
          console.log('We have permission to send push notifications');
          that.pushSetup();
        } else {
          console.log('We do not have permission to send push notifications');
        }
      });
  }

  backBtnHandler() {
    this.platform.registerBackButtonAction(() => {

      let nav = this.app.getActiveNavs()[0];
      let activeView = nav.getActive();

      if (activeView.name == 'TabsPage') {

        if (nav.canGoBack()) { //Can we go back?
          nav.pop();
        } else {
          const alert = this.alertCtrl.create({
            title: 'App termination',
            message: 'Do you want to close the app?',
            buttons: [{
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                console.log('Application exit prevented!');
              }
            }, {
              text: 'Close App',
              handler: () => {
                this.platform.exitApp(); // Close this application
              }
            }]
          });
          alert.present();
        }
      }
    });
  }

  pushSetup() {
    // to initialize push notifications
    let that = this;
    const options: PushOptions = {
      android: {
        senderID: '644983599736',
        icon: 'safety365',
        iconColor: 'red'
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'true'
      },
      windows: {},
      browser: {
        pushServiceURL: 'http://push.api.phonegap.com/v1/push'
      }
    };

    const pushObject: PushObject = that.push.init(options);


    pushObject.on('notification').subscribe((notification: any) => {
      // if (localStorage.getItem("notifValue") != null) {
      //   if (localStorage.getItem("notifValue") == 'true') {
          this.tts.speak(notification.message)
            .then(() => console.log('Success'))
            .catch((reason: any) => console.log(reason));
      //   }
      // }
      
      if (notification.additionalData.foreground) {
        const toast = this.toastCtrl.create({
          message: notification.message,
          duration: 3000,
          position: 'top'
        });
        toast.present();
      }
    });

    pushObject.on('registration')
      .subscribe((registration: any) => {
        console.log("device token => " + registration.registrationId);
        localStorage.setItem("DEVICE_TOKEN", registration.registrationId)
        this.storage.set("DEVICE_TOKEN", registration.registrationId);
      });

    pushObject.on('error').subscribe(error => {
      console.error('Error with Push plugin', error)
    });
  }

  initializeApp() {
    let that = this;
    that.platform.ready().then(() => {
      that.pushNotify();
      that.backBtnHandler();
    });
  }
}
